from music21 import *
import numpy as np
import librosa
import librosa.display
import os
from keras.models import Model
from keras.initializers import glorot_uniform
import tensorflow as tf
from keras.layers import Input,Dropout,  Dense,Reshape,LSTM
from keras.callbacks import ReduceLROnPlateau
from sklearn.model_selection import train_test_split
from tensorflow.python.client import device_lib

class Lstm_Jazz:
    def __init__(self,directory,librosa_path,resample, offset, pas, leny, number_freq, n_fft,memory_frac,points_to_cut,hop_length,
                 loss):

        """

        :param directory: where your jazz songs are
        :param librosa_path: a list where the path to executables are of muse and librosa
        :param resample: the resampling frequencies
        :param number_freq: the n most powerful frequencies you want to take
        :param n_fft: the number of points for the fft
        :param memory_frac: fraction of the memory
        :param points_to_cut: claps and low sound at the end and beginning of jazz songs, cut those
        :param pas: the length of the vector we learn from (10s for learning?)
        :param leny: the length of the prediction (prediction on 0.5 seconds?)
        :param offset:  offset for generating the vector
        :param loss: loss function for training
        """

        self.directory = directory
        self.musepath = librosa_path[0]
        self.musicxml = librosa_path[1]
        self.resample = resample
        self.number_freq = number_freq
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.memory_frac = memory_frac
        self.pas = pas
        self.leny= leny
        self.offset = offset
        self.points_to_cut = points_to_cut
        self.loss= loss

        self.run_config(self.memory_frac)

    # 0.5 s time window , freq resol = 2hz
    def run_config(self ,memory_frac=0.7):
        """
        :return: just runs the set up for librosa path and GPU %
        """
        environment.set('musescoreDirectPNGPath', self.musepath)
        environment.set("musicxmlPath", self.musicxml)
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = memory_frac
        session = tf.Session(config=config)
        print(device_lib.list_local_devices())


    def create_freq_mag_phase(self,arr, freq=False):
        """

        :param arr: sampled signal
        :param freq: either counts with bins (False) or convert to frequencies
        :return: From a sampled signal , returns a Phase Amplitude matrix from Short Fast Fourier transform
        with only the N-number_freq most powerful frequencies from the spectrum

        """

        D = librosa.stft(arr, n_fft=self.n_fft, center=False, window=np.ones(self.n_fft))

        S, phase = librosa.magphase(D)

        Power = np.sort(np.abs(S.T), axis=1)[:, - self.number_freq:]

        index_freq = np.argsort(np.abs(S.T))[:, - self.number_freq:]

        if freq == True:
            frequencies = (self.resample / self.n_fft) * index_freq
        else:

            frequencies = index_freq.astype(int)


        return frequencies, Power

    @staticmethod
    def secs2frame(seconds,n_fft,resample,hop_length ):
        """
        :param seconds : how many seconds you want
        :return: transforms it to stft time bins
        """
        if hop_length == None:
            ratio = 4
        else:
            ratio = hop_length/n_fft
        return int((resample/n_fft)*seconds*ratio)

    @staticmethod
    def concat_matrices(frequencies,Power):
        Stacker = np.concatenate((  frequencies,Power) , axis =1)

        return Stacker

    def create_array_mag_phase(self,Xr):
        """

        :param Xr: The matrix of all the frequencies and power retained
        :return: A matrix whose dimensions are (nbrows, pas, number_freq)
        This function turns a frequency & power vs time bins into a matrix with dimension :
        (number of examples , learning sequence, number of n- most powerful frequencies for the learning sequence) for X
        (number of examples , number of n- most powerful frequencies of the next time bin) for Y
        """
        len_signal = Xr.shape[0]

        X_cut = Xr[self.pointstocut:len_signal - self.pointstocut, :]

        nbrows = int((X_cut.shape[0] - self.pas - self.leny) // self.offset)

        X = np.zeros((1, self.pas, self.number_freq))

        Y = np.zeros((1, self.leny, self.number_freq))
        for i in range(0, nbrows):
            cond1 = (np.where(X_cut[i * self.offset: i * self.offset + self.pas,
                              self.number_freq:2 * self.number_freq] < 1)[0].shape[0]) > 0
            cond2 = (np.where(X_cut[i * self.offset: i * self.offset + self.pas,
                              self.number_freq:2 * self.number_freq] > 260)[0].shape[0]) > 0

            cond3 = (np.where(X_cut[i * self.offset: i * self.offset + self.pas, :self.number_freq] < 40)[0].shape[0]) > 0
            cond4 = (np.where(X_cut[i * self.offset: i * self.offset + self.pas, :self.number_freq] > 250)[0].shape[0]) > 0

            if cond3 | cond4 | cond1 | cond2:
                pass
            else:
                xx = X_cut[i * self.offset: i * self.offset + self.pas, :self.number_freq].reshape((1, self.pas, self.number_freq))
                yy = X_cut[(i + 1) * self.offset + self.pas:(i + 1) * self.offset + self.pas + self.leny,
                     :self.number_freq].reshape(
                    (1, self.leny, self.number_freq))

                X = np.append(X, xx, axis=0)
                Y = np.append(Y, yy, axis=0)

        X = X[1:, :, :]
        Y = Y[1:, :, :]



        return X, Y

    def stack_XY(self):
        """
        :return: read all Jazz files and compute X & Y matrices thanks to former functions
        """

        list_files = os.listdir(self.directory)
        X = np.zeros((1, self.pas, self.number_freq))
        Y = np.zeros((1, self.leny, self.number_freq))

        for files in list_files:

            try:
                print('processing song title {}'.format(files))
                signal, sr = librosa.load(self.directory + '/' + files, sr=resample)

                frequencies, Power = self.create_freq_mag_phase(signal, self.n_fft,
                                                           self.number_freq, self.resample,
                                                           self.hop_length, freq=False)

                Xr = Lstm_Jazz.concat_matrices(frequencies, Power)

                Xtemp, Ytemp = self.create_array_mag_phase(Xr)


                X = np.append(X, Xtemp, axis=0)
                Y = np.append(Y, Ytemp, axis=0)


            except Exception as e:
                print('Exception', e)
                pass

        X = X[1:, :, :]
        Y = Y[1:, :, :]



        return X, Y


    def MyLSTM(self,input_shape):
        """

        :param input_shape: input shape for model
        :return: generates the model
        """
        input_layer = Input(shape=input_shape, name='main')

        first_layer = LSTM(512, return_sequences=True, activation='relu',
                           init=glorot_uniform(),
                           name='first_layer')(input_layer)

        second_layer = LSTM(512, return_sequences=True, activation='relu',
                             init=glorot_uniform(),
                             name='second_layer')(first_layer)

        third_layer = LSTM(512, activation='relu',
                            init=glorot_uniform(),
                            name='third_layer')(second_layer)

        fourth_layer = Dense(512, activation='relu', init=glorot_uniform(), name='fourth_layer')(third_layer)
        fourth_layer_d = Dropout(0.3)(fourth_layer)

        fifth_layer = Dense(512, activation='relu', init=glorot_uniform(), name='fifth_layer')(fourth_layer_d)
        fifth_layer_d = Dropout(0.3)(fifth_layer)

        sixth_layer = Dense(self.leny * self.number_freq, activation='relu', init=glorot_uniform(), name='sixth_layer')(
            fifth_layer_d)
        output_i = Dropout(0.3)(sixth_layer)

        output = Reshape((self.leny, self.number_freq), name='Reshapor1')(output_i)

        myLstm = Model(inputs=input_layer, output=output, name='LSTM_jaz_model')


        return myLstm

    def start_training(self,X_trains,Y_trains,epochs=50,batch_size=1000):
        """

        :param X_trains: training matrix of examples
        :param Y_trains: training matrix of targets
        :param epochs: epochs to train
        :param batch_size: batch size
        :return:
        """
        MyLSTM_model = self.MyLSTM(input_shape=(self.pas, self.number_freq))
        # On vient compiler le modèle #
        MyLSTM_model.compile(loss=self.loss, optimizer='adam')

        # On regarde à quoi ressemble le modèle #
        MyLSTM_model.summary()

        # Callback pour adapter le learning rate en fonction des performances #
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=3, min_lr=0.0001)

        MyLSTM_model.fit(X_trains,Y_trains,epochs=epochs,batch_size= batch_size
                 ,callbacks=[reduce_lr]
                )

        self.myLSTM_model = MyLSTM_model

    def predict_song(self, X , t,  Ystd, Ymean, Xstd, Xmean):
        """

        :param X: the database
        :param t: the number of songs to generate
        :param Ystd: std of Y
        :param Ymean: mean of Y
        :param Xstd: std of X
        :param Xmean: mean of X
        :return:
        """
        randominteger = np.random.randint(0, X.shape[0])
        number = int(np.floor(t / (self.n_fft / self.resample)))
        line = X[randominteger, :, :]
        arr = np.array([])
        output_notes = []

        offset = 0
        for j in range(0, number):

            line = (line - Xmean) / Xstd
            points = self.myLSTM_model.predict(line.reshape((1, self.pas, self.number_freq)))

            for i in range(0, points.shape[1]):
                Y = (points * Ystd + Ymean)[0, i, :]
                notes = []
                (detectedPitchObjects, listPlot) = audioSearch.pitchFrequenciesToObjects(
                    Y, useScale=scale.ChromaticScale('G'))

                if np.where(Y > 0)[0].shape[0] > 1:
                    notes = []
                    for mynote in [str(p) for p in detectedPitchObjects]:
                        new_note = note.Note(mynote)
                        new_note.storedInstrument = instrument.Piano()
                        notes.append(new_note)
                    new_chord = chord.Chord(notes)
                    new_chord.offset = self.offset
                    output_notes.append(new_chord)
                # pattern is a note
                else:
                    for mynote in [str(p) for p in detectedPitchObjects]:
                        new_note = note.Note(mynote)
                        new_note.offset = self.offset

                        new_note.storedInstrument = instrument.Piano()
                        output_notes.append(new_note)
                offset += 0.35


            line = np.concatenate((line * Xstd + Xmean, (points * Ystd + Ymean).reshape((self.leny, self.number_freq))),
                                  axis=0)[-self.pas:]

        midi_stream = stream.Stream(output_notes)

        return midi_stream


#### Running thr main.py ####

n_fft = 9000
resample = 18000
hop_length = n_fft
number_freq = 2
#The idea here is to get at least twice the frequency of the highest-pitched instrument (Shannon)
# Then define on how many seconds we want to do the STFT , let's assume than half a second is relevant ,
# it could be also one second, but then our n_fft = 9000 for half a second sample.
# Then we want to take the 2 most powerful frequencies (as in Jazz they also play many times to notes)

# Generally there are claps at the end of a song and the beginning is not loud at all so we cut 30 s from each song

leny  ,pas , offset ,pointstocut  = ( Lstm_Jazz.secs2frame(0.5,n_fft,resample,hop_length) ,
                                     Lstm_Jazz.secs2frame(20,n_fft,resample,hop_length),
                                                    Lstm_Jazz.secs2frame(0.5,n_fft,resample,hop_length),
                                                    Lstm_Jazz.secs2frame(30,n_fft,resample,hop_length))
## Instanciate the object ##
myLstm_Jazz = Lstm_Jazz('Jazz',['C:\Program Files (x86)\MuseScore 2\\bin\\MuseScore.exe',
                                 'C:\Program Files (x86)\MuseScore 2\\bin\\MuseScore.exe'],
                         resample , offset , pas , leny,number_freq,n_fft,0.7,pointstocut,hop_length,
                        "mean_squared_error")


## Compute X ,Y examples ##
X , Y = myLstm_Jazz.stack_XY()

## train test split ##

X_train,X_test,Y_train,Y_test = train_test_split(X,Y,test_size= 0.3)

Xmean , Xstd = np.mean(X_train,axis=0) , np.std(X_train,axis =0)
Ymean , Ystd = np.mean(Y_train ,axis=0), np.std(Y_train,axis=0)

X_trains = ((X_train - Xmean)/Xstd)
X_tests = ((X_test - Xmean)/Xstd)

Y_trains = (Y_train - Ymean)/Ystd
Y_tests = (Y_test - Ymean)/Ystd

## You can visualize here the distributions (recommended) ##

## then train the model ##

myLstm_Jazz.start_training(X_trains,Y_trains,epochs=50,batch_size=1000)

## Then predict some songs ##
# generate 10 songs #
t = 10

myLstm_Jazz.predict_song( X , t,  Ystd, Ymean, Xstd, Xmean)